package com.braciakoala.kinoserver.rest;

import com.braciakoala.kinoserver.document.DocumentComponent;
import com.braciakoala.kinoserver.dto.ErrorDto;
import com.braciakoala.kinoserver.dto.ErrorMessage;
import com.braciakoala.kinoserver.dto.OrderDto;
import com.braciakoala.kinoserver.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Kontroler do obsługi dokumentów.
 */
@RestController
@RequestMapping("/api/documents")
public class DocumentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentController.class);

    @Autowired
    public DocumentController(DocumentComponent documentComponent, OrderService orderService) {
        this.documentComponent = documentComponent;
        this.orderService = orderService;
    }

    /**
     * Komponent obsługujący dokumenty.
     */
    private final DocumentComponent documentComponent;
    /**
     * Serwis obsługujący zamówienia.
     */
    private final OrderService orderService;

    /**
     * Metoda tworząca dokument za pomocą obiektu otrzymanego w zapytaniu.
     * @param order Obiekt zamówienia, na postwie którego tworzony jest dokument.
     * @return Zwraca obiekt zamówienia. W razie niepowodznia zwraca zwraca kod błedu wraz z wiadomością.
     */
    @CrossOrigin
    @PostMapping(value = "/create-document")
    public ResponseEntity<?> createDocument(
            @RequestBody OrderDto order)
    {
        LOGGER.info("--- creating document for order {}... ", order.getId());

        boolean success = false;
        try {
            success = documentComponent.createDocument(order);
        }catch (IOException io) {
            LOGGER.error("Error creating document: {}", io.getMessage());
        }

        ErrorDto errorMessage = new ErrorDto(ErrorMessage.ERROR_CREATE.getErrorMessage());
        HttpStatus errorCode = ErrorMessage.ERROR_CREATE.getErrorCode();

        return success ?
                new ResponseEntity<>(order, HttpStatus.CREATED) :
                new ResponseEntity<>(errorMessage, errorCode);
    }

    /**
     * Metoda tworząca dokument za pomocą obiektu otrzymanego w zapytaniu.
     * @param id ID zamówienia z serwera, na postwie którego tworzony jest dokument.
     * @return Zwraca obiekt zamówienia. W razie niepowodznia zwraca zwraca kod błedu wraz z wiadomością.
     */
    @CrossOrigin
    @GetMapping(value = "/create-document/{id}")
    public ResponseEntity<?> createDocument(
            @PathVariable("id") Long id)
    {
        LOGGER.info("--- creating document for order {}... ", id);

        OrderDto orderDto = null;
        boolean success = false;
        try {
            orderDto = orderService.find(id);
            success = documentComponent.createDocument(orderDto);
        } catch (IOException io) {
            LOGGER.error("Error creating document: {}", io.getMessage());
        }

        ErrorDto errorMessage = new ErrorDto(ErrorMessage.ERROR_CREATE.getErrorMessage());
        HttpStatus errorCode = ErrorMessage.ERROR_CREATE.getErrorCode();

        return success ?
                new ResponseEntity<>(orderDto, HttpStatus.CREATED) :
                new ResponseEntity<>(errorMessage, errorCode);
    }

}

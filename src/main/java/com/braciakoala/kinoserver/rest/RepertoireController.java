package com.braciakoala.kinoserver.rest;

import com.braciakoala.kinoserver.dto.ErrorDto;
import com.braciakoala.kinoserver.dto.ErrorMessage;
import com.braciakoala.kinoserver.dto.FilmDto;
import com.braciakoala.kinoserver.dto.ScreeningDto;
import com.braciakoala.kinoserver.service.FilmService;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/repertoire")
public class RepertoireController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepertoireController.class);

    @Autowired
    public RepertoireController(ScreeningService screeningService, FilmService filmService) {
        this.screeningService = screeningService;
        this.filmService = filmService;
    }

    /**
     * Serwis obsługujący seanse filmowe.
     */
    private final ScreeningService screeningService;

    /**
     * Serwis obsługujący filmy.
     */
    private final FilmService filmService;

    /**
     * Metoda testowa zwracająca przykładowy obiekt klasy FilmDto.
     * @return Zwraca przykładowy obiekt filmu.
     */
    @CrossOrigin
    @GetMapping(value = "/film-test")
    public FilmDto getFilm()
    {
        LOGGER.info("--- getting film... ");

        return filmService.getExample();
    }

    /**
     * Metoda pobierająca ID seansu z serwera oraz zwracająca obiekt tego seansu.
     * @param id ID seansu.
     * @return W razie powodzenia, zwraca przesłany obiekt seansu wraz z kodem 200.
     * W przeciwnym wypadku zwraca błąd z odpowiednim kodem HTTP.
     */
    @CrossOrigin
    @GetMapping(value = "/screening/{id}")
    public ResponseEntity<?> getScreening(@PathVariable("id") Long id)
    {
        LOGGER.info("--- getting screening {}", id);

        ScreeningDto screeningDto = screeningService.find(id);

        ErrorDto errorMessage = new ErrorDto(ErrorMessage.ERROR_INDEX.getErrorMessage());
        HttpStatus errorCode = ErrorMessage.ERROR_INDEX.getErrorCode();

        return screeningDto != null ?
                new ResponseEntity<>(screeningDto, HttpStatus.OK) :
                new ResponseEntity<>(errorMessage, errorCode);
    }

    /**
     * Metoda zwracająca wszystkie obiekty seansów z serwera.
     * @return W razie powodzenia, zwraca liste obiektów seansów wraz z kodem 200.
     * W przeciwnym wypadku zwraca błąd z odpowiednim kodem HTTP.
     */
    @CrossOrigin
    @GetMapping(value = "/get-all-screenings")
    public ResponseEntity<List<ScreeningDto>> getScreenings() {
        LOGGER.info("--- getting screenings...");

        List<ScreeningDto> list = null;

        try {
            list = screeningService.findAll();
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return list == null || list.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NO_CONTENT) :
                new ResponseEntity<>(list, HttpStatus.OK);
    }

    /**
     * Metoda edytująca wybrany obiekt seansu z serwera.
     * @param id ID seansu do edycji.
     * @param screening Obiekt, którym ma być zastąpiony poprzedni seans.
     * @return W razie powodzenia, zwraca obiekt seansu wraz z kodem 200.
     * W przeciwnym wypadku zwraca błąd z odpowiednim kodem HTTP.
     */
    @CrossOrigin
    @PutMapping(value = "/screening-edit/{id}")
    public ResponseEntity<?> editScreening(
            @PathVariable("id") Long id,
            @RequestBody ScreeningDto screening)
    {
        LOGGER.info("--- editing screening {}", id);

        ScreeningDto screeningDto = screeningService.edit(id, screening);

        ErrorDto errorMessage = new ErrorDto(ErrorMessage.ERROR_INDEX.getErrorMessage());
        HttpStatus errorCode = ErrorMessage.ERROR_INDEX.getErrorCode();

        return screeningDto != null ?
                new ResponseEntity<>(screeningDto, HttpStatus.OK) :
                new ResponseEntity<>(errorMessage, errorCode);
    }

}

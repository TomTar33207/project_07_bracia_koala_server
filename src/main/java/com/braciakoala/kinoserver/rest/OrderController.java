package com.braciakoala.kinoserver.rest;

import com.braciakoala.kinoserver.document.DocumentComponent;
import com.braciakoala.kinoserver.dto.*;
import com.braciakoala.kinoserver.service.OrderService;
import com.braciakoala.kinoserver.service.ProductService;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Kontroler do obsługi zamówień.
 */
@RestController
@RequestMapping("/api/orders")
public class OrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    public OrderController(OrderService orderService, ProductService productService) {
        this.orderService = orderService;
        this.productService = productService;
    }

    /**
     * Serwis obsługujący zamówienia.
     */
    private final OrderService orderService;
    /**
     * Serwis obsługujący produkty.
     */
    private final ProductService productService;

    /**
     * Metoda testowa zwracająca przykładowy obiekt klasy OrderDto.
     * @return Zwraca przykładowy obiekt zamówienia.
     */
    @CrossOrigin
    @GetMapping(value = "/order-test")
    public OrderDto getOrder()
    {
        LOGGER.info("--- getting test order... ");

        return orderService.getExample();
    }

    /**
     * Metoda tworząca zamówienie. Rezerwuje miejsca na seans, wpisuje zamówienie do zbioru zamówień oraz
     * generuje bilet jako plik PDF.
     * @param order Obiekt zamówienia.
     * @return W razie powodzenia, zwraca przesłany obiekt zamówienia wraz z kodem 201.
     * W przeciwnym wypadku zwraca błąd z odpowiednim kodem HTTP.
     */
    @CrossOrigin
    @PostMapping(value = "/create-order")
    public ResponseEntity<?> createOrder(@RequestBody OrderDto order)
    {
        LOGGER.info("--- creating order... ");

        ErrorMessage errorMessage = null;
        try {
            errorMessage = orderService.makeOrder(order);
        } catch (IOException e) {
            LOGGER.error("Error creating document! {}", e.getMessage());
        }

        return errorMessage == null ?
                new ResponseEntity<>(order, HttpStatus.CREATED) :
                new ResponseEntity<>(new ErrorDto(errorMessage.getErrorMessage()), errorMessage.getErrorCode());
    }

    /**
     * Metoda zwracająca liste produktów przechowywanych na serwerze.
     * @return Zwraca liste produktów lub kod błedu w razie niepowodzenia.
     */
    @CrossOrigin
    @GetMapping(value = "/get-all-products")
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        LOGGER.info("--- getting list of products...");

        List<ProductDto> list = null;

        try {
            list = productService.findAll();
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return list == null || list.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NO_CONTENT) :
                new ResponseEntity<>(list, HttpStatus.OK);
    }

}

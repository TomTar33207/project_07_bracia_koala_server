package com.braciakoala.kinoserver.service;

import com.braciakoala.kinoserver.dto.ErrorMessage;
import com.braciakoala.kinoserver.dto.OrderDto;

import java.io.IOException;
import java.util.List;

/**
 * Serwis dla zamówień
 */
public interface OrderService {
    String fileName = "orders.csv";

    OrderDto find(Long id);
    List<OrderDto> findAll();
    OrderDto create(Long id, OrderDto newOrder);

    ErrorMessage makeOrder(OrderDto order) throws IOException;

    OrderDto getExample();

    Long getCurrentId();
}

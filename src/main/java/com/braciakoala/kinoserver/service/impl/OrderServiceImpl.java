package com.braciakoala.kinoserver.service.impl;

import com.braciakoala.kinoserver.document.DocumentComponent;
import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.ErrorDto;
import com.braciakoala.kinoserver.dto.ErrorMessage;
import com.braciakoala.kinoserver.dto.OrderDto;
import com.braciakoala.kinoserver.dto.ScreeningDto;
import com.braciakoala.kinoserver.service.OrderService;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    public OrderServiceImpl(Resource<OrderDto> orderResource,
                            DocumentComponent documentComponent, ScreeningService screeningService) {
        this.orderResource = orderResource;
        this.documentComponent = documentComponent;
        this.screeningService = screeningService;
    }

    private final Resource<OrderDto> orderResource;
    private final DocumentComponent documentComponent;
    private final ScreeningService screeningService;

    @Override
    public OrderDto find(Long id) {
        return orderResource.find(id);
    }

    @Override
    public List<OrderDto> findAll() {
        return orderResource.findAll();
    }

    @Override
    public OrderDto create(Long id, OrderDto newOrder) {
        return orderResource.create(id, newOrder);
    }

    @Override
    public ErrorMessage makeOrder(OrderDto order) throws IOException {
        OrderDto orderDto = null;
        ScreeningDto screeningToEdit = screeningService.find(order.getScreeningId());
        boolean screeningEdited = screeningToEdit != null && screeningService.applyOrder(screeningToEdit, order);

        if (screeningEdited) {
             orderDto = create(getCurrentId(), order);
            if (orderDto != null) {
                documentComponent.createDocument(order);
            }
        }

        if (orderDto == null)
            return screeningEdited ? ErrorMessage.ERROR_CREATE : ErrorMessage.ERROR_SEATS;
        else
            return null;
    }

    @Override
    public OrderDto getExample() {
        return orderResource.getExample();
    }

    @Override
    public Long getCurrentId() {
        List<OrderDto> screenings = findAll();

        Long max = -1L;
        for (OrderDto sd : screenings) {
            if (sd.getId() > max)
                max = sd.getId();
        }
        return max + 1;
    }
}

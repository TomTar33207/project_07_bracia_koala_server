package com.braciakoala.kinoserver.service.impl;

import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.ProductDto;
import com.braciakoala.kinoserver.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    public ProductServiceImpl(Resource<ProductDto> productResource) {
        this.productResource = productResource;
    }

    private Resource<ProductDto> productResource;

    @Override
    public ProductDto find(Long id) {
        return productResource.find(id);
    }

    @Override
    public List<ProductDto> findAll() {
        return  productResource.findAll();
    }
}

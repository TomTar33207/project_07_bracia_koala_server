package com.braciakoala.kinoserver.service.impl;

import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.OrderDto;
import com.braciakoala.kinoserver.dto.ScreeningDto;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScreeningServiceImpl implements ScreeningService {

    @Autowired
    public ScreeningServiceImpl(Resource<ScreeningDto> screeningResource) {
        this.screeningResource = screeningResource;
    }

    private Resource<ScreeningDto> screeningResource;

    @Override
    public ScreeningDto find(Long id) {
        return screeningResource.find(id);
    }

    @Override
    public List<ScreeningDto> findAll() {
        return screeningResource.findAll();
    }

    @Override
    public ScreeningDto edit(Long id, ScreeningDto newScreening) {
        return screeningResource.edit(id, newScreening);
    }

    @Override
    public boolean applyOrder(ScreeningDto screeningDto, OrderDto order) {
        ScreeningDto copy = new ScreeningDto(screeningDto);
        try {
            for (Integer index : order.getNormalSeats()) {
                if (!screeningDto.getNormalSeats()[index])
                    return false;
                screeningDto.getNormalSeats()[index] = false;
            }
            for (Integer index : order.getVipSeats()) {
                if (!screeningDto.getVipSeats()[index])
                    return false;
                screeningDto.getVipSeats()[index] = false;
            }
            edit(screeningDto.getId(), screeningDto);
            return true;
        }
        catch (Exception e){
            System.err.println(e.getMessage());
            // if failed reverse changes
            edit(copy.getId(), copy);
            return false;
        }
    }
}

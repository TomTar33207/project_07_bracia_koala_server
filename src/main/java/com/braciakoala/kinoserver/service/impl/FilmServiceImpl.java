package com.braciakoala.kinoserver.service.impl;

import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.FilmDto;
import com.braciakoala.kinoserver.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    public FilmServiceImpl(Resource<FilmDto> filmDtoResource) {
        this.filmDtoResource = filmDtoResource;
    }

    Resource<FilmDto> filmDtoResource;

    @Override
    public FilmDto find(Long id) {
        return filmDtoResource.find(id);
    }

    @Override
    public FilmDto getExample() {
        return filmDtoResource.getExample();
    }
}

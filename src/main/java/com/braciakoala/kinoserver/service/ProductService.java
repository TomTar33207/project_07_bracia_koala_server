package com.braciakoala.kinoserver.service;

import com.braciakoala.kinoserver.dto.ProductDto;

import java.util.List;

/**
 * Serwis dla produktów
 */
public interface ProductService {
    String fileName = "products.csv";

    ProductDto find(Long id);
    List<ProductDto> findAll();
}

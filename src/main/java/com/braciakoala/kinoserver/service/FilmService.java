package com.braciakoala.kinoserver.service;

import com.braciakoala.kinoserver.dto.FilmDto;

/**
 * Serwis dla filmów
 */
public interface FilmService {
    String fileName = "films.csv";

    FilmDto find(Long id);

    FilmDto getExample();
}

package com.braciakoala.kinoserver.service;

import com.braciakoala.kinoserver.dto.OrderDto;
import com.braciakoala.kinoserver.dto.ScreeningDto;
import java.util.List;

/**
 * Serwis dla seansów
 */
public interface ScreeningService {
    String fileName = "screenings.csv";

    ScreeningDto find(Long id);
    List<ScreeningDto> findAll();
    ScreeningDto edit(Long id, ScreeningDto newScreening);
    boolean applyOrder(ScreeningDto screeningDto, OrderDto order);
}

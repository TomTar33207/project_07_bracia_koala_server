package com.braciakoala.kinoserver.document;

import com.braciakoala.kinoserver.dto.OrderDto;

import java.io.IOException;

/**
 * Komponent dla dokumentów
 */
public interface DocumentComponent {
    boolean createDocument(OrderDto orderData) throws IOException;
    String getImgPath(String imgName);
}

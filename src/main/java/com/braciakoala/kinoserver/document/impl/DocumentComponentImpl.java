package com.braciakoala.kinoserver.document.impl;

import com.braciakoala.kinoserver.dto.ProductDto;
import com.braciakoala.kinoserver.dto.ScreeningDto;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import com.braciakoala.kinoserver.document.DocumentComponent;
import com.braciakoala.kinoserver.dto.OrderDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;

@Component
public class DocumentComponentImpl implements DocumentComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentComponentImpl.class);

    @Autowired
    public DocumentComponentImpl(ScreeningService screeningService) {
        this.screeningService = screeningService;
    }

    ScreeningService screeningService;

    @Override
    public boolean createDocument(OrderDto orderData) throws IOException {

        // TODO if seats are already taken, return false

        PDDocument document = new PDDocument();
        PDPageContentStream contentStream = null;
        try {
            PDPage page = new PDPage();
            document.addPage(page);
            ScreeningDto screeningDto = screeningService.find(orderData.getScreeningId());
            contentStream = new PDPageContentStream(document, page);
            String cinema = "KINO";

            InputStream stream = getClass().getResourceAsStream("/AbhayaLibre-Medium.ttf");
            PDFont font = PDType0Font.load(document, stream);

            float width = font.getStringWidth(cinema) / 1000 * 24;

            contentStream.setFont(font, 24);
            contentStream.beginText();
            contentStream.newLineAtOffset(
                    (page.getMediaBox().getWidth() - width) / 2,
                    page.getMediaBox().getHeight() - 50
            );
            contentStream.showText(cinema);
            contentStream.endText();

            PDImageXObject logo = PDImageXObject.createFromFile(getImgPath("logo.png"), document);
            contentStream.drawImage(
                    logo,
                    (page.getMediaBox().getWidth() - logo.getWidth()) / 2,
                    page.getMediaBox().getHeight() - logo.getHeight() - 75
            );

            //font = PDType1Font.TIMES_ROMAN;
            String film = screeningDto.getFilm().getTitle();
            width = font.getStringWidth(film) / 1000 * 20;

            contentStream.setFont(font, 20);
            contentStream.beginText();
            contentStream.newLineAtOffset(
                    (page.getMediaBox().getWidth() - width) / 2,
                    page.getMediaBox().getHeight() - 375
            );
            contentStream.showText(film);
            contentStream.endText();

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            film = format1.format(screeningDto.getDate().getTime());
            width = font.getStringWidth(film) / 1000 * 20;
            contentStream.beginText();
            contentStream.newLineAtOffset(
                    (page.getMediaBox().getWidth() - width) / 2,
                    page.getMediaBox().getHeight() - 400
            );
            contentStream.showText(film);
            contentStream.endText();

            format1 = new SimpleDateFormat("dd.MM");
            film = format1.format(screeningDto.getDate().getTime());

            String vipSeats = "Miejsca VIP: ";
            for (Integer i: orderData.getVipSeats())
                vipSeats += (i+1) + ", ";
            contentStream.setFont(font, 12);
            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 425);
            contentStream.showText(vipSeats);
            contentStream.endText();

            String normalSeats = "Miejsca normalne: ";
            for (Integer i: orderData.getNormalSeats())
                normalSeats += (i+1) + ", ";
            contentStream.setFont(font, 12);
            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 437);
            contentStream.showText(normalSeats);
            contentStream.endText();

            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 450);
            contentStream.showText("Zamówione napoje i przekaski:");
            contentStream.endText();

            int count = 0;
            for (ProductDto productDto : orderData.getProducts()) {
                if (productDto.getQuantity() > 0) {
                    count++;
                    contentStream.beginText();
                    contentStream.newLineAtOffset(
                            100,
                            page.getMediaBox().getHeight() - 450 - count * 15
                    );
                    contentStream.showText(productDto.getName() + " x" + productDto.getQuantity());
                    contentStream.endText();
                }
            }

            count++;
            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 450 - count * 15);
            contentStream.showText("Cena: " + orderData.getCost());
            contentStream.endText();

            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 625);
            contentStream.showText("Dane klienta:");
            contentStream.endText();

            contentStream.setFont(font, 18);
            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 650);
            contentStream.showText(orderData.getName() + " " + orderData.getSurname());
            contentStream.endText();

            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 675);
            contentStream.showText("Numer telefonu: " + orderData.getPhoneNumber());
            contentStream.endText();

            contentStream.beginText();
            contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 700);
            contentStream.showText("Adres e-mail: " + orderData.getEmailAddress());
            contentStream.endText();

            contentStream.close();

            String myDocumentPath = System.getProperty("user.home") + "/Documents";
            document.save(new File(myDocumentPath + "/bilet_" + orderData.getId() + "_" + film + ".pdf"));

        }
        catch (IOException io) {
            io.printStackTrace();
            return false;
        }
        finally {
            document.close();
        }

        return true;
    }

    @Override
    public String getImgPath(String imgName) {
        try {
            String path = getClass().getResource("/img/" + imgName).getPath();
            path = path.replaceFirst("/", "");
            return java.net.URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        }catch (UnsupportedEncodingException uee) {
            LOGGER.error("Can't decode path to file {}", imgName);
            return null;
        }
    }
}

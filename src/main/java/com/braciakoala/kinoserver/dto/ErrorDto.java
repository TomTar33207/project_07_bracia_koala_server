package com.braciakoala.kinoserver.dto;

/**
 * Klasa reprezentująca błąd serwera.
 */
public class ErrorDto {
    private String errorMessage;

    public ErrorDto(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}

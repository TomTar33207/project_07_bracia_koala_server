package com.braciakoala.kinoserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Klasa reprezentująca zamówienie
 */
public class OrderDto
{
    @JsonProperty("id")
    private Long id;
    @JsonProperty("screeningId")
    private Long screeningId;

    @JsonProperty("normalSeats")
    private Integer[] normalSeats;
    @JsonProperty("vipSeats")
    private Integer[] vipSeats;

    @JsonProperty("name")
    private String name;
    @JsonProperty("surname")
    private String surname;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("emailAddress")
    private String emailAddress;

    @JsonProperty("products")
    private ProductDto[] products;

    @JsonProperty("cost")
    private int cost;

    public OrderDto() { }

    public OrderDto(
            Long id,
            Long screeningId,
            Integer[] normalSeats,
            Integer[] vipSeats,
            String name,
            String surname,
            String phoneNumber,
            String emailAddress,
            ProductDto[] products,
            int cost) {
        this.id = id;
        this.screeningId = screeningId;
        this.normalSeats = normalSeats;
        this.vipSeats = vipSeats;
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.products = products;
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public int getCost() {
        return cost;
    }

    public Long getScreeningId() {
        return screeningId;
    }

    public Integer[] getNormalSeats() {
        return normalSeats;
    }

    public Integer[] getVipSeats() {
        return vipSeats;
    }

    public ProductDto[] getProducts() {
        return products;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String result;
        result = id + "," + screeningId + ",";

        if (normalSeats.length != 0) {
            for (Integer seat : normalSeats) {
                result += seat + ";";
            }
            result = result.substring(0, result.length() - 1);
        }
        result += ",";
        if (vipSeats.length != 0) {
            for (Integer seat : vipSeats) {
                result += seat + ";";
            }
            result = result.substring(0, result.length() - 1);
        }

        result += "," + name + "," + surname + "," + phoneNumber + "," + emailAddress + ",";

        for (ProductDto productDto : products) {
            result += productDto.getId() + ";" + productDto.getQuantity() + "_";
        }
        result = result.substring(0, result.length() - 1);

        result += "," + cost;

        return result;
    }
}

package com.braciakoala.kinoserver.dto;

import org.springframework.http.HttpStatus;

/**
 * Typ wyliczeniowy reprezentujący wiadomość błędu.
 */
public enum ErrorMessage {

    ERROR_CREATE(
            "Error creating object!",
            HttpStatus.INTERNAL_SERVER_ERROR
    ),
    ERROR_INDEX(
            "Can't find element!",
            HttpStatus.BAD_REQUEST
    ),
    ERROR_SEATS(
            "Seats are already taken!",
            HttpStatus.ALREADY_REPORTED
    );

    private String errorMessage;

    private HttpStatus errorCode;

    ErrorMessage(String errorMessage, HttpStatus errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public HttpStatus getErrorCode() {
        return errorCode;
    }
}

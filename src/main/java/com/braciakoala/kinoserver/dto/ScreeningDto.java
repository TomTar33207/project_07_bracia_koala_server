package com.braciakoala.kinoserver.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Klasa reprezentująca seans
 */
public class ScreeningDto
{
    @JsonProperty("id")
    private Long id;
    @JsonProperty("film")
    private FilmDto film;

    @JsonProperty("date")
    private String date_string;

    @JsonIgnore
    private Calendar date;

    @JsonProperty("normalSeats")
    private boolean[] normalSeats;
    @JsonProperty("vipSeats")
    private boolean[] vipSeats;
    @JsonProperty("normalPrice")
    private int normalPrice;
    @JsonProperty("vipPrice")
    private int vipPrice;

    public ScreeningDto() { }

    public ScreeningDto(ScreeningDto screeningDto) {
        this.id = screeningDto.getId();
        this.film = screeningDto.getFilm();
        this.date_string = screeningDto.getDate_string();
        this.date = screeningDto.getDate();
        this.normalSeats = screeningDto.getNormalSeats();
        this.vipSeats = screeningDto.getVipSeats();
        this.normalPrice = screeningDto.getNormalPrice();
        this.vipPrice = screeningDto.getVipPrice();
    }

    public ScreeningDto(Long id, FilmDto film, String date, boolean[] normal, boolean[] vip, int normalPrice, int vipPrice){
        this.id = id;
        this.film = film;

        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            cal.setTime(sdf.parse(date));
            this.date_string = date;
            this.date = cal;
        }
        catch (ParseException pe) {
            System.err.println(pe.getMessage());
            this.date = Calendar.getInstance();
        }

        this.normalSeats = new boolean[normal.length];
        this.normalSeats = normal;

        this.vipSeats = new boolean[vip.length];
        this.vipSeats = vip;

        this.normalPrice = normalPrice;
        this.vipPrice = vipPrice;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Calendar getDate() {
        return date;
    }
    @JsonIgnore
    public String getDate_string(){
        return date_string;
    }

    public FilmDto getFilm() {
        return film;
    }

    public boolean[] getNormalSeats() {
        return normalSeats;
    }

    public boolean[] getVipSeats() {
        return vipSeats;
    }

    public int getNormalPrice() {
        return normalPrice;
    }

    public int getVipPrice() {
        return vipPrice;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        String result = "";

        result += id + "_";

        result += film.getId() + "_";

        result += date_string + "_";

        for (int i = 0; i < normalSeats.length; i++) {
            result += normalSeats[i];
            result += (i != normalSeats.length-1) ? ";" : "";
        }
        result += "_";

        for (int i = 0; i < vipSeats.length; i++) {
            result += vipSeats[i];
            result += (i != vipSeats.length-1) ? ";" : "";
        }
        result += "_";

        result += normalPrice + "_";

        result += vipPrice;

        return result;
    }
}

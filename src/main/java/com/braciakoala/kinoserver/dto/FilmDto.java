package com.braciakoala.kinoserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Klasa reprezentująca film dostępny w kinie
 */
public class FilmDto
{
    @JsonProperty("id")
    private Long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("yearOfProduction")
    private int yearOfProduction;
    @JsonProperty("genre")
    private String genre;

    public FilmDto() { }

    public FilmDto(Long id, String title, String desc, int year, String genre){
        this.id = id;
        this.title = title;
        this.description = desc;
        this.yearOfProduction = year;
        this.genre = genre;
    }

    public Long getId() {
        return id;
    }

    public String getGenre(){
        return genre;
    }

    public String getTitle(){
        return title;
    }

    public String getDescription(){
        return description;
    }

    public int getYearOfProduction(){ return yearOfProduction; }

    @Override
    public String toString() {
        return title + ";" + description + ";" + yearOfProduction + ";" + genre;
    }
}

package com.braciakoala.kinoserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Klasa reprezentująca produkt (napój, przekąska)
 */
public class ProductDto {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private int price;
    @JsonProperty("quantity")
    private int quantity;

    public ProductDto() { }

    public ProductDto(Long id, String name, int price, int quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

package com.braciakoala.kinoserver.domain.model.dao;

import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.FilmDto;
import com.braciakoala.kinoserver.dto.ScreeningDto;
import com.braciakoala.kinoserver.service.FilmService;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Repository
public class ScreeningDao implements Resource<ScreeningDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScreeningDao.class);

    @Autowired
    public ScreeningDao(FilmService filmService) {
        this.filmService = filmService;
    }

    FilmService filmService;

    @Override
    public ScreeningDto find(Long id) {
        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            for (String line: lines) {
                String[] data = line.split("_");
                if (data[0].equals(id.toString())) {
                    return convertLineToData(data);
                }
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    ScreeningService.fileName,
                    Paths.get(getPath())
            );
            return null;
        }

        return null;
    }

    @Override
    public List<ScreeningDto> findAll() {
        List<ScreeningDto> seance = new ArrayList<>();

        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            for (final String line: lines) {
                ScreeningDto fileData = convertLineToData(line.split("_"));
                seance.add(fileData);
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    ScreeningService.fileName,
                    Paths.get(getPath())
            );
        }

        return seance;
    }

    @Override
    public ScreeningDto create(Long id, ScreeningDto newSeans) {
        return null;
    }

    @Override
    public ScreeningDto edit(Long id, ScreeningDto newSeans) {
        boolean found = false;
        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            int lineNumber = 0;

            for (String line: lines) {
                String[] data = line.split("_");
                if (data[0].equals(id.toString())) {
                    // replace line
                    found = true;
                    newSeans.setId(id);
                    lines.set(lineNumber, newSeans.toString());
                    Files.write(Paths.get(getPath()), lines, StandardCharsets.UTF_8);
                }
                lineNumber++;
            }
        }
        catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    ScreeningService.fileName,
                    Paths.get(getPath())
            );
            return null;
        }

        return (found) ? newSeans : null;
    }

    @Override
    public ScreeningDto getExample() {
        return null;
    }


    @Override
    public ScreeningDto convertLineToData(final String[] line) {
        long id = -1L;
        FilmDto filmDto = new FilmDto();
        Calendar calendar = Calendar.getInstance();
        boolean[] normalSeats = new boolean[0];
        boolean[] vipSeats = new boolean[0];
        int normalPrice = 0;
        int vipPrice = 0;

        try {
            // id
            id = Long.parseLong(line[0]);

            // film
            filmDto = filmService.find(Long.parseLong(line[1]));

            // date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.setTime(sdf.parse(line[2]));

            // normalSeats
            String[] nSeats = line[3].split(";");
            normalSeats = new boolean[nSeats.length];
            for (int i = 0; i < nSeats.length; i++) {
                normalSeats[i] = Boolean.parseBoolean(nSeats[i]);
            }

            // vipSeats
            String[] vSeats = line[4].split(";");
            vipSeats = new boolean[vSeats.length];
            for (int i = 0; i < vSeats.length; i++) {
                vipSeats[i] = Boolean.parseBoolean(vSeats[i]);
            }

            // normalPrice
            normalPrice = Integer.parseInt(line[5]);

            // vipPrice
            vipPrice = Integer.parseInt(line[6]);
        }
        catch (Exception e) {
            LOGGER.error("Failed to parse data! {}", e.getMessage());
        }

        ScreeningDto screeningDto = new ScreeningDto(
                id,
                filmDto,
                line[2],
                normalSeats,
                vipSeats,
                normalPrice,
                vipPrice
        );
        screeningDto.setDate(calendar);
        return screeningDto;
    }

    @Override
    public String getPath() {
        try {
            String path = getClass().getResource("/csv/" + ScreeningService.fileName).getPath();
            path = path.replaceFirst("/", "");
            return java.net.URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        }
        catch (UnsupportedEncodingException uee) {
            LOGGER.error("Can't decode path to file {}", ScreeningService.fileName);
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

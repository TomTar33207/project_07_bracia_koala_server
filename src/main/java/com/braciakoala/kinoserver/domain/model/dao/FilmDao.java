package com.braciakoala.kinoserver.domain.model.dao;

import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.FilmDto;
import com.braciakoala.kinoserver.service.FilmService;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Repository
public class FilmDao implements Resource<FilmDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDao.class);

    @Override
    public FilmDto find(Long id) {
        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            for (String line: lines) {
                String[] data = line.split("_");
                if (data[0].equals(id.toString())) {
                    return convertLineToData(data);
                }
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    FilmService.fileName,
                    Paths.get(getPath())
            );
            return null;
        }

        return null;
    }

    @Override
    public List<FilmDto> findAll() {
        return null;
    }

    @Override
    public FilmDto create(Long id, FilmDto newObj) {
        return null;
    }

    @Override
    public FilmDto edit(Long id, FilmDto newObj) {
        return null;
    }

    @Override
    public FilmDto getExample() {
        return new FilmDto(
                1L,
                "Jason Bourne",
                "description",
                2016,
                "action"
        );
    }

    @Override
    public FilmDto convertLineToData(String[] line) {
        return new FilmDto(
                Long.parseLong(line[0]),
                line[1],
                line[2],
                Integer.parseInt(line[3]),
                line[4]
        );
    }

    @Override
    public String getPath() {
        try {
            String path = getClass().getResource("/csv/" + FilmService.fileName).getPath();
            path = path.replaceFirst("/", "");
            return java.net.URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        }catch (UnsupportedEncodingException uee) {
            LOGGER.error("Can't decode path to file {}", FilmService.fileName);
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

package com.braciakoala.kinoserver.domain.model.dao;

import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.OrderDto;
import com.braciakoala.kinoserver.dto.ProductDto;
import com.braciakoala.kinoserver.service.OrderService;
import com.braciakoala.kinoserver.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderDao implements Resource<OrderDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDao.class);

    @Autowired
    public OrderDao(ProductService productService) {
        this.productService = productService;
    }

    ProductService productService;

    @Override
    public OrderDto find(Long id) {
        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            for (String line: lines) {
                String[] data = line.split(",");
                if (data[0].equals(id.toString())) {
                    return convertLineToData(data);
                }
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    OrderService.fileName,
                    Paths.get(getPath())
            );
            return null;
        }

        return null;
    }

    @Override
    public List<OrderDto> findAll() {
        List<OrderDto> orders = new ArrayList<>();

        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            for (final String line: lines) {
                OrderDto fileData = convertLineToData(line.split(","));
                orders.add(fileData);
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    OrderService.fileName,
                    Paths.get(getPath())
            );
        }

        return orders;
    }

    @Override
    public OrderDto create(Long id, OrderDto newOrder) {
        try {
            newOrder.setId(id);
            Files.write(Paths.get(getPath()), ("\n" + newOrder.toString()).getBytes(), StandardOpenOption.APPEND);
            return newOrder;

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    OrderService.fileName,
                    Paths.get(getPath())
            );
            return null;
        }
        catch (Exception e) {
            LOGGER.error("Error while creating order: {}",
                    e.getMessage()
            );
            return null;
        }
    }

    @Override
    public OrderDto edit(Long id, OrderDto newOrder) {
        return null;
    }

    @Override
    public OrderDto getExample() {
        return new OrderDto(
                1L,
                17L,
                new Integer[]{24,25},
                new Integer[0],
                "Jan",
                "Kowalski",
                "123987456",
                "j.kowalski@gmail.com",
                new ProductDto[]{new ProductDto(3L, "precel", 9, 3)},
                56
        );
    }

    @Override
    public OrderDto convertLineToData(String[] line) {
        Integer[] normalSeats = new Integer[0];
        Integer[] vipSeats = new Integer[0];
        List<ProductDto> products = new ArrayList<>();

        try {
            // normal seats
            if (!line[2].equals("")) {
                String[] normalSeatsArray = line[2].split(";");
                normalSeats = new Integer[normalSeatsArray.length];
                for (int i = 0; i < normalSeatsArray.length; i++) {
                    normalSeats[i] = Integer.parseInt(normalSeatsArray[i]);
                }
            }

            // vip seats
            if (!line[3].equals("")) {
                String[] vipSeatsArray = line[3].split(";");
                vipSeats = new Integer[vipSeatsArray.length];
                for (int i = 0; i < vipSeatsArray.length; i++) {
                    vipSeats[i] = Integer.parseInt(vipSeatsArray[i]);
                }
            }

            // products
            String[] productsSet = line[8].split("_");
            for (String p: productsSet) {
                String[] pData = p.split(";");
                ProductDto productDto = productService.find(Long.parseLong(pData[0]));
                productDto.setQuantity(Integer.parseInt(pData[1]));
                products.add(productDto);
            }
        }
        catch (Exception e) {
            LOGGER.error("Failed to parse data! {}", e.getMessage());
        }

        return new OrderDto(
                Long.parseLong(line[0]),
                Long.parseLong(line[1]),
                normalSeats,
                vipSeats,
                line[4],
                line[5],
                line[6],
                line[7],
                products.toArray(new ProductDto[0]),
                Integer.parseInt(line[9])
        );
    }

    @Override
    public String getPath() {
        try {
            String path = getClass().getResource("/csv/" + OrderService.fileName).getPath();
            path = path.replaceFirst("/", "");
            return java.net.URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        }catch (UnsupportedEncodingException uee) {
            LOGGER.error("Can't decode path to file {}", OrderService.fileName);
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

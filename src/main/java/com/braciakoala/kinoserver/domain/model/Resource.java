package com.braciakoala.kinoserver.domain.model;

import java.util.List;

/**
 * Interfejs dodający metody do obsługi zasobu przechowywanego na serwerze.
 * @param <T> Klasa zasobu.
 */
public interface Resource<T> {
    /**
     * Metoda szukająca zasobu.
     * @param id ID szukanego zasobu.
     * @return Zwraca szukany zasób lub null w razie niepowodzenia.
     */
    T find(Long id);

    /**
     * Metoda szukająca wszystkich zasobów.
     * @return Zwraca wszystkie zasoby lub null w razie niepowodzenia.
     */
    List<T> findAll();

    /**
     * Metoda tworząca nowy zasób przechowywany na serwerze.
     * @param id ID nowego zasobu.
     * @param newObj Obiekt nowego zasobu.
     * @return Zwraca nowy zasób lub null w razie niepowodzenia.
     */
    T create(Long id, T newObj);

    /**
     * Metoda modyfikująca istniejący zasób przechowywany na serwerze.
     * @param id ID modyfikowanego zasobu.
     * @param newObj Obiekt nowego zasobu.
     * @return Zwraca zmodyfikowany zasób lub null w razie niepowodzenia.
     */
    T edit(Long id, T newObj);

    T getExample();

    /**
     * Metoda konwertująca linie pliku na obiekt klasy T.
     * @param line Tablica tekstów przechowujących wartości kolejnych pól klasy.
     * @return Zwraca przekonwertowany obiekt klasy T.
     */
    T convertLineToData(final String[] line);

    /**
     * Metoda wyznacza ścieżke do pliku .csv
     * @return Zwraca globalną ścieżke do pliku
     */
    String getPath();
}

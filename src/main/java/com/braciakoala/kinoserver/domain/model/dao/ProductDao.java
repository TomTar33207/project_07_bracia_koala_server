package com.braciakoala.kinoserver.domain.model.dao;

import com.braciakoala.kinoserver.domain.model.Resource;
import com.braciakoala.kinoserver.dto.OrderDto;
import com.braciakoala.kinoserver.dto.ProductDto;
import com.braciakoala.kinoserver.service.OrderService;
import com.braciakoala.kinoserver.service.ProductService;
import com.braciakoala.kinoserver.service.ScreeningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDao implements Resource<ProductDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDao.class);

    @Override
    public ProductDto find(Long id) {
        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            for (String line: lines) {
                String[] data = line.split(",");
                if (data[0].equals(id.toString())) {
                    return convertLineToData(data);
                }
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    ProductService.fileName,
                    Paths.get(getPath())
            );
            return null;
        }

        return null;
    }

    @Override
    public List<ProductDto> findAll() {
        List<ProductDto> orders = new ArrayList<>();

        try {
            final List<String> lines = Files.readAllLines(Paths.get(getPath()), StandardCharsets.UTF_8);

            for (final String line: lines) {
                ProductDto fileData = convertLineToData(line.split(","));
                orders.add(fileData);
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found at path : {}",
                    ProductService.fileName,
                    Paths.get(getPath())
            );
        }

        return orders;
    }

    @Override
    public ProductDto create(Long id, ProductDto newSeans) {
        return null;
    }

    @Override
    public ProductDto edit(Long id, ProductDto newSeans) {
        return null;
    }

    @Override
    public ProductDto getExample() {
        return null;
    }

    @Override
    public ProductDto convertLineToData(String[] line) {
        return new ProductDto(
                Long.parseLong(line[0]),
                line[1],
                Integer.parseInt(line[2]),
                1
        );
    }

    @Override
    public String getPath() {
        try {
            String path = getClass().getResource("/csv/" + ProductService.fileName).getPath();
            path = path.replaceFirst("/", "");
            return java.net.URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        }catch (UnsupportedEncodingException uee) {
            LOGGER.error("Can't decode path to file {}", ProductService.fileName);
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
